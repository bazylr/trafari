//
//  Constants.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 19/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

struct Constants {
    struct Color {
        static let jobOfferTitle = UIColor(red: 0.0, green: 107.0/255.0, blue: 207.0/255.0, alpha: 1.0)
    }
}


//
//  JobOffer.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 17/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

class JobOffer: NSObject, Codable {
    var jobID: Int?
    var title: String?
    var advertiser: String?
    var location: String?
    var status: String?
    var salary: String?
    var isExternal: Bool?
    var listedDate: String?
    var savedDate: String?
    var links: Data?
    lazy var isExpired: Bool = {
        return status == "Expired"
    }()
    
    
    enum CodingKeys:String,CodingKey {
        case jobID = "jobId"
        case title
        case advertiser
        case location
        case status
        case salary
        case isExternal
        case listedDate = "listedDateUtc"
        case savedDate = "savedDateUtc"
    }
}

//  Copyright © 2017 SEEK. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SavedJobsClientType {
    func getSavedJobs(page:Int, completionHandler: @escaping (JSON?, Error?) -> Void)
}

//Returns a the mock data stored in SavedJobsResponse.json
class SavedJobsClient: SavedJobsClientType {
    
    public func getSavedJobs(page:Int, completionHandler: @escaping (JSON?, Error?) -> Void) {
        
        if let file = Bundle.main.url(forResource: "SavedJobsResponsePage" + String(page) , withExtension: "json"),
            let data = try? Data(contentsOf: file, options: .mappedIfSafe),
            let json = try? JSON(data: data) {
            return completionHandler(json, nil)
        } else {
            completionHandler(nil, NSError(domain: "codingChallengeError", code: 123, userInfo: [:]))
            return
            
        }
    }
}

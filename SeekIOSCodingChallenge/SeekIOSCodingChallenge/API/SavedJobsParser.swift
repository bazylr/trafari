//
//  SavedJobsParser.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 17/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol SavedJobsParserType {
    func parseSavedJobs(json: JSON) -> ([JobOffer]?, Int?)
}

class SavedJobsParser: SavedJobsParserType {

    func parseSavedJobs(json: JSON) -> ([JobOffer]?, Int?) {
        let jobs = json["jobs"]
        let nextPage = json["nextPage"].int
        var jobOfferObjects = [JobOffer]()
        jobs.forEach { _, dict in
            if let data = try? dict.rawData(),
                let jobOffer = try? JSONDecoder().decode(JobOffer.self, from: data) {
                jobOfferObjects.append(jobOffer)
            }
        }
        return (jobOfferObjects, nextPage)
    }
}

//
//  API.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 17/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

protocol APIProtocol {
    func getJobOfferList(refresh: Bool, completionHandler: @escaping (([JobOffer]?) -> Void))
}

class API: NSObject, APIProtocol {

    var savedJobsClient: SavedJobsClientType
    var savedJobsParser: SavedJobsParserType
    var alertPresenter: AlertPresenterType
    var nextPage: Int? = 1
    
    init(savedJobsClient: SavedJobsClientType = SavedJobsClient(),
            savedJobsParser: SavedJobsParserType = SavedJobsParser(),
            alertPresenter: AlertPresenterType = AlertPresenter()) {
        self.savedJobsClient = savedJobsClient
        self.savedJobsParser = savedJobsParser
        self.alertPresenter = alertPresenter
        super.init()
    }
    
    func getJobOfferList(refresh: Bool, completionHandler: @escaping (([JobOffer]?) -> Void)) {
        if refresh {
            nextPage = 1
        }
        guard let nextPage = nextPage else { return }
        savedJobsClient.getSavedJobs(page: nextPage) { [weak self] (json, error) in
            
            if let json = json,
                let (jobOffers, nextPage) = self?.savedJobsParser.parseSavedJobs(json: json) {
                self?.nextPage = nextPage
                completionHandler(jobOffers)
            } else {
                self?.alertPresenter.presentAlert(with: "failed to load".localized)
            }
            
        }
    }
}

//
//  AlertPresenter.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 21/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

protocol AlertPresenterType {
    func presentAlert(with title: String)
}

class AlertPresenter: AlertPresenterType {

    var currentViewController: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
    
    func presentAlert(with title: String = "Error".localized) {
        let alert = UIAlertController(title: "Error".localized,
                                      message: title,
                                      preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok".localized, style: .default, handler: nil)
        alert.addAction(action)
        currentViewController?.present(alert, animated: true, completion:nil)
    }
    
}

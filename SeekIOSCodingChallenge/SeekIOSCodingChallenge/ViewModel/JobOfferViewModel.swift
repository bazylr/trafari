//
//  JobOfferViewModel.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 18/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

class JobOfferViewModel: NSObject {
    var title: String = ""
    var advertiser: String?
    var location: String?
    var listedDate: String?
    var savedDate: String?
    var isExternal: Bool?
    var isExpired: Bool
    
    init(jobOffer: JobOffer) {
        
        title += jobOffer.title ?? ""
        advertiser = jobOffer.advertiser
        location = jobOffer.location
        if let listedDateFormatted = jobOffer.listedDate?.formattedDate {
            listedDate = "Job posted".localized + listedDateFormatted
        }
        if let savedDateFormatted = jobOffer.savedDate?.formattedDate {
            savedDate = "Saved".localized + savedDateFormatted
        }
        isExpired = jobOffer.isExpired
        isExternal = jobOffer.isExternal
        super.init()
    }
}

//
//  SavedJobsViewModel.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 17/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

class SavedJobsViewModel: NSObject {

    private var api: APIProtocol
    private var jobOfferViewModels = [JobOfferViewModel]()

    public var reloadDataCompletionHandler: (()->())?
    
    init(api: APIProtocol = API()) {
        self.api = api
        super.init()
    }

    public var numberOfSections: Int {
        return 1
    }
    
    public var numberOfCells: Int {
        return jobOfferViewModels.count
    }
    
    public func fetch(_ refresh: Bool = false) {
        if refresh == true {
            jobOfferViewModels = [JobOfferViewModel]()
        }
        api.getJobOfferList(refresh: refresh) { [weak self] jobOffers in
            guard let strongSelf = self else  { return }
            jobOffers?.forEach({ jobOffer in
               let viewModel = strongSelf.createJobOfferViewModel(jobOffer: jobOffer)
                strongSelf.jobOfferViewModels.append(viewModel)
            })
            strongSelf.reloadDataCompletionHandler?()
        }
    }
    
    private func createJobOfferViewModel(jobOffer: JobOffer) -> JobOfferViewModel {
        let jobOfferViewModel = JobOfferViewModel(jobOffer: jobOffer)
        return jobOfferViewModel
    }
    
    public func getJobOfferViewModel(at indexPath: IndexPath) -> JobOfferViewModel {
        return jobOfferViewModels[indexPath.row]
    }
}

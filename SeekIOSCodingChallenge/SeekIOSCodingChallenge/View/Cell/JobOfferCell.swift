//
//  JobOfferCell.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 17/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

class JobOfferCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var advertiserLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var listedDateLabel: UILabel!
    @IBOutlet weak var savedDateLabel: UILabel!
    @IBOutlet weak var expiredImageView: UIImageView!
    var viewModel: JobOfferViewModel?
    
    func setup(with viewModel: JobOfferViewModel) {
        self.viewModel = viewModel
        setTitleLabel()
        advertiserLabel.text = viewModel.advertiser ?? ""
        locationLabel.text = viewModel.location ?? ""
        listedDateLabel.text = viewModel.listedDate ?? ""
        savedDateLabel.text = viewModel.savedDate ?? ""
        expiredImageView.isHidden = !viewModel.isExpired
    }
    
    func setTitleLabel() {
        guard let jobOfferTitle = viewModel?.title,
                let isExpired = viewModel?.isExpired else { return }
        var customizedJobOfferTitle = jobOfferTitle
        if isExpired {
            customizedJobOfferTitle = "expired".localized + jobOfferTitle
        }
        let attributedText = NSMutableAttributedString.init(string: customizedJobOfferTitle)
        if let range = customizedJobOfferTitle.range(of: jobOfferTitle) {
            let nsrange = NSRange(range, in: customizedJobOfferTitle)
            attributedText.addAttribute(.foregroundColor,
                                        value: Constants.Color.jobOfferTitle,
                                        range: nsrange)
        }
        titleLabel.attributedText = attributedText
    }
    
}

extension StringProtocol where Index == String.Index {
    func nsRange(from range: Range<Index>) -> NSRange {
        return NSRange(range, in: self)
    }
}

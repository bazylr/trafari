//
//  String+DateFormatting.swift
//  SeekIOSCodingChallenge
//
//  Created by Bazyl Reinstein on 18/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import UIKit

extension String {
    var formattedDate: String? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
        guard let date = dateFormatterGet.date(from: self) else { return nil }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy"
        let endString = dateFormatter.string(from: date)
        return endString
    }
    
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
    
}

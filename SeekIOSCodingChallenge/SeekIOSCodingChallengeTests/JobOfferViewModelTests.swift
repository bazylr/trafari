//
//  JobOfferViewModelTests.swift
//  SeekIOSCodingChallengeTests
//
//  Created by Bazyl Reinstein on 21/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import XCTest
@testable import SeekIOSCodingChallenge

class JobOfferViewModelTests: XCTestCase {
    
    func testJobOfferViewModelCreation() {
        let jobOffer = JobOffer()
        jobOffer.jobID = 007
        jobOffer.title = "Title"
        jobOffer.advertiser = "Advertiser"
        jobOffer.location = "Location"
        jobOffer.status = "Expired"
        jobOffer.salary = "Salary"
        jobOffer.isExternal = true
        jobOffer.listedDate = "2017-06-02T05:31:46.77Z"
        jobOffer.savedDate = "2018-06-02T05:31:46.77Z"
        
        let viewModel = JobOfferViewModel(jobOffer: jobOffer)
        XCTAssertTrue(viewModel.title == "Title")
        XCTAssertTrue(viewModel.advertiser == "Advertiser")
        XCTAssertTrue(viewModel.location == "Location")
        XCTAssertTrue(viewModel.isExternal == true)
        XCTAssertTrue(viewModel.isExpired == true)
        XCTAssertTrue(viewModel.savedDate! == "Saved 02 Jun 2018")
        XCTAssertTrue(viewModel.listedDate! == "Job posted 02 Jun 2017")
    }
    
    func testEmptyJobOfferViewModelCreation() {
        let jobOffer = JobOffer()
        
        let viewModel = JobOfferViewModel(jobOffer: jobOffer)
        XCTAssertTrue(viewModel.title == "")
        XCTAssertNil(viewModel.advertiser)
        XCTAssertNil(viewModel.location)
        XCTAssertNil(viewModel.isExternal)
        XCTAssertFalse(viewModel.isExpired)
        XCTAssertNil(viewModel.savedDate)
        XCTAssertNil(viewModel.listedDate)
        
    }
    
}


//var title: String = ""
//var advertiser: String?
//var location: String?
//var listedDate: String?
//var savedDate: String?
//var isExternal: Bool?
//var isExpired: Bool

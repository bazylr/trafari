//
//  SavedJobsClientTests.swift
//  SeekIOSCodingChallengeTests
//
//  Created by Bazyl Reinstein on 20/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import SeekIOSCodingChallenge

class SavedJobsClientTests: XCTestCase {
    
    var client = SavedJobsClient()
    
    func testClientDownloadedData() {
        client.getSavedJobs(page:1) { data, error in
            XCTAssertTrue(data?.count == 2)
            let offers = data?["jobs"]
            XCTAssertTrue(offers?.count == 19)
        }
    }
    
}

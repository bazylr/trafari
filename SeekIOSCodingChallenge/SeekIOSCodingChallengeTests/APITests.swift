//
//  APITests.swift
//  SeekIOSCodingChallengeTests
//
//  Created by Bazyl Reinstein on 21/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import XCTest
@testable import SeekIOSCodingChallenge

class APITests: XCTestCase {
    
    let api = API()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testAPIDownload() {
        api.getJobOfferList(refresh: false) { [weak self] offers in
            guard let offers = offers else { return }
            XCTAssertTrue(offers.count == 19)
            let offer = offers[0]
            XCTAssertTrue(offer.title == "Customer Service Consultant")
            XCTAssertTrue(offer.advertiser == "Link Group")
            XCTAssertTrue(offer.location == "Melbourne")
            XCTAssertTrue(offer.status == "Expired")
            XCTAssertTrue(offer.isExpired == true)
            XCTAssertTrue(offer.isExternal == true)
            XCTAssertTrue(offer.savedDate == "2017-06-07T23:57:06.5Z")
            XCTAssertTrue(offer.listedDate == "2017-06-02T05:31:46.77Z")
            XCTAssertTrue(self?.api.nextPage == 2)
            self?.api.getJobOfferList(refresh: false) { [weak self] offers in
                guard let offers = offers else { return }
                XCTAssertTrue(offers.count == 3)
                XCTAssertNil(self?.api.nextPage)
            }
        }
    }
    
}

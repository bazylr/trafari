//
//  SavedJobsViewModelTests.swift
//  SeekIOSCodingChallengeTests
//
//  Created by Bazyl Reinstein on 21/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import XCTest
@testable import SeekIOSCodingChallenge

class SavedJobsViewModelTests: XCTestCase {
    
    func testViewModel() {
        let viewModel = SavedJobsViewModel(api: MockAPI())
        XCTAssertTrue(viewModel.numberOfCells == 0)
        XCTAssertTrue(viewModel.numberOfSections == 1)
        viewModel.fetch()
        XCTAssertTrue(viewModel.numberOfCells == 30)
        viewModel.fetch()
        XCTAssertTrue(viewModel.numberOfCells == 60)
        viewModel.fetch(true)
        XCTAssertTrue(viewModel.numberOfCells == 30)
    }
}

class MockAPI: APIProtocol {
    func getJobOfferList(refresh: Bool, completionHandler: @escaping (([JobOffer]?) -> Void)) {
        let jobOffers = Array(repeating: JobOffer(), count: 30)
        completionHandler(jobOffers)
    }
}

//
//  String+DateFormattingTests.swift
//  SeekIOSCodingChallengeTests
//
//  Created by Bazyl Reinstein on 21/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import XCTest
@testable import SeekIOSCodingChallenge

class String_DateFormattingTests: XCTestCase {
    
    func test7June() {
        let dateString = "2017-06-07T23:57:01.513Z"
        let formattedDate = dateString.formattedDate
        XCTAssertTrue(formattedDate == "07 Jun 2017")
    }
    
    func test1Jan() {
        let dateString = "2017-01-01T23:57:01.513Z"
        let formattedDate = dateString.formattedDate
        XCTAssertTrue(formattedDate == "01 Jan 2017")
    }
    
    func testWrongDate() {
        let dateString = "2017-06-32T23:57:01.513Z"
        let formattedDate = dateString.formattedDate
        XCTAssertNil(formattedDate)
    }
    
}

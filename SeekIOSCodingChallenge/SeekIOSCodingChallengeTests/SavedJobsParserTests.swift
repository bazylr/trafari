//
//  SavedJobsParserTests.swift
//  SeekIOSCodingChallengeTests
//
//  Created by Bazyl Reinstein on 21/08/2018.
//  Copyright © 2018 SEEK. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import SeekIOSCodingChallenge

class SavedJobsParserTests: XCTestCase {
    
    let parser = SavedJobsParser()
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testParsing() {
        let json: JSON = [
            "jobs": [[
            "jobId": 33602575,
            "title": "Customer Service Consultant",
            "advertiser": "Link Group",
            "location": "Melbourne",
            "status": "Expired",
            "salary": "",
            "isExternal": true,
            "listedDateUtc": "2017-06-02T05:31:46.77Z",
            "savedDateUtc": "2017-06-07T23:57:06.5Z",
            "_links": [
            "delete": [
            "href": "https://mapi.cloud.seek.com.au/my-activity/saved-jobs/33602575"
            ]
            ]
            ]],
            "nextPage": 22
        ]
        let (jobOffers, nextPage) = parser.parseSavedJobs(json: json)
        guard let unwrappedJobOffers = jobOffers else { return }
        XCTAssertTrue(unwrappedJobOffers.count == 1)
        XCTAssertTrue(nextPage == 22)
        
    }
    
}
